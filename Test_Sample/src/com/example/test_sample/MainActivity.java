/*
 * Copyright 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.example.test_sample;

import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.test_sample.NavAdapter.OnNavImageClickListener;
import com.example.test_sample.NavAdapter.OnNavImageClickListener1;

@SuppressLint("NewApi")
public class MainActivity extends Activity {

	static Cursor c;
	// Context context2;
	public Database1 database1 = new Database1(this);
	private DrawerLayout mDrawerLayout;
	private ListView mDrawerList;
	private ActionBarDrawerToggle mDrawerToggle;

	private CharSequence mDrawerTitle;
	private CharSequence mTitle;
	// private String[] mPlanetTitles;
	public ArrayList<String> listItems = new ArrayList<String>();
	// public ArrayAdapter<String> adapter;
	public static String string;
	public static TextView nub, display;
	public int positn;
	public static String text;
	public String userinput, string3;
	String id1 = "0";
	private NavAdapter mAdapter;
	public static String textValue;
	public static int count = 0;
	public static Button incre;
	public static Button decr;
	public static Button save;
	public static Button cancel;
	public static EditText user_message;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		mTitle = mDrawerTitle = getTitle();
		mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
		mDrawerList = (ListView) findViewById(R.id.left_drawer);

		// set a custom shadow that overlays the main content when the drawer
		// opens
		mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);

		listItems.add("add new item");
		// adapter = new ArrayAdapter<String>(this, R.layout.rowlayout,
		// R.id.label, listItems);
		mAdapter = new NavAdapter(this.getApplicationContext(), listItems);
		mDrawerList.setAdapter(mAdapter);

		mDrawerList.setOnItemClickListener(new DrawerItemClickListener());

		// getting db values
		database1.open();
		c = database1.getvalues();

		if (c != null) {
			if (c.moveToFirst()) {
				do {

					String index = c.getString(0).toString();
					String listname = c.getString(1).toString();
					String value = c.getString(2).toString();
					listItems.add(listname);
					database1.close();
				} while (c.moveToNext());

			}

		}

		// nub.getText().toString();

		// enable ActionBar app icon to behave as action to toggle nav drawer
		getActionBar().setDisplayHomeAsUpEnabled(true);
		getActionBar().setHomeButtonEnabled(true);

		// ActionBarDrawerToggle ties together the the proper interactions
		// between the sliding drawer and the action bar app icon
		mDrawerToggle = new ActionBarDrawerToggle(this, /* host Activity */
		mDrawerLayout, /* DrawerLayout object */
		R.drawable.ic_drawer, /* nav drawer image to replace 'Up' caret */
		R.string.drawer_open, /* "open drawer" description for accessibility */
		R.string.drawer_close /* "close drawer" description for accessibility */
		) {
			public void onDrawerClosed(View view) {
				getActionBar().setTitle(mTitle);
				invalidateOptionsMenu(); // creates call to //
											// onPrepareOptionsMenu()
				incre.setVisibility(View.VISIBLE);
				decr.setVisibility(View.VISIBLE);
				nub.setVisibility(View.VISIBLE);
				save.setVisibility(View.VISIBLE);
				cancel.setVisibility(View.VISIBLE);
				user_message.setVisibility(View.VISIBLE);
				display.setVisibility(View.INVISIBLE);
			}

			public void onDrawerOpened(View drawerView) {
				getActionBar().setTitle(mDrawerTitle);
				invalidateOptionsMenu(); // creates call to
				// onPrepareOptionsMenu()
			}
		};
		mDrawerLayout.setDrawerListener(mDrawerToggle);

		if (savedInstanceState == null) {
			selectItem(0);
		}

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.main, menu);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// The action bar home/up action should open or close the drawer.
		// ActionBarDrawerToggle will take care of this.
		if (mDrawerToggle.onOptionsItemSelected(item)) {
			return true;
		}

		// Handle action buttons
		switch (item.getItemId()) {

		case R.id.deleteall:
			database1.open();
			database1.deleteall();
			database1.close();
			// listItems.removeAll(listItems);
			// mAdapter.notifyDataSetChanged();

		case R.id.view:
			Intent i = new Intent(getApplicationContext(), Listview.class);
			startActivity(i);
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	/* The click listner for ListView in the navigation drawer */
	private class DrawerItemClickListener implements ListView.OnItemClickListener {
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position, final long id) {

			text = parent.getItemAtPosition(position).toString();
			selectItem(position);
			positn = position;
			mDrawerList.setItemChecked(position, true);
			setTitle(text);
			mDrawerLayout.closeDrawer(mDrawerList);
			mAdapter = new NavAdapter(getApplicationContext(), listItems);
			mDrawerList.setAdapter(mAdapter);

			mAdapter.setOnImageClickListener(new OnNavImageClickListener() {

				@Override
				public void onClick(View view, Object item) {
					database1.open();
					c = database1.getvalues();
					if (c != null) {
						if (c.moveToFirst()) {
							do {
								final String string2 = c.getString(c.getColumnIndex("name"));
								// Log.d("hello frns", string2);
								// Log.d("text value", item.toString());
								string3 = item.toString();

								if (string2.equals(string3)) {

									AlertDialog.Builder builder2 = new AlertDialog.Builder(MainActivity.this);
									final EditText input = new EditText(MainActivity.this);
									input.setHeight(100);
									input.setWidth(340);
									input.setGravity(Gravity.LEFT);
									input.setText(c.getString(1).toString());
									input.setImeOptions(EditorInfo.IME_ACTION_DONE);
									builder2.setMessage("Press OK or Cancel");
									builder2.setView(input);
									builder2.setPositiveButton("OK", new DialogInterface.OnClickListener() {

										@Override
										public void onClick(DialogInterface dialog, int which) {
											// input.setText("Hello");
											userinput = input.getText().toString();
											int index = positn;
											listItems.set(index, userinput);
											mAdapter = new NavAdapter(getApplicationContext(), listItems);
											mDrawerList.setAdapter(mAdapter);
											database1.open();
											long updateid1;
											updateid1 = database1.Updatelistvalues(string3, userinput);
										}
									});
									builder2.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
										@Override
										public void onClick(DialogInterface dialog, int which) {

										}
									});
									builder2.show();
								}
							} while (c.moveToNext());
						}

					}

				}
			});

			mAdapter.setOnImageClickListener1(new OnNavImageClickListener1() {

				@Override
				public void onClick(View view, final Object item) {

					AlertDialog.Builder b = new AlertDialog.Builder(MainActivity.this);
					b.setIcon(android.R.drawable.ic_dialog_alert);
					b.setMessage("Delete this from history?");
					b.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int whichButton) {

							database1.open();
							c = database1.getvalues();
							if (c != null) {
								if (c.moveToFirst()) {
									do {
										final String string2 = c.getString(c.getColumnIndex("name"));

										Log.d("hello frns", string2);
										Log.d("text value", item.toString());
										string3 = item.toString();

										if (string2.equals(string3)) {
											listItems.remove(item);
											listItems.remove(database1.deleteone(string3));
											mAdapter.notifyDataSetChanged();
											Toast.makeText(getApplicationContext(), "deleted", Toast.LENGTH_LONG).show();
										}

									} while (c.moveToNext());

								}
							}

						}
					});
					b.setNegativeButton("No", new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int whichButton) {
							dialog.cancel();
						}
					});

					b.show();
				}
			});
			// -------//

			if (text.equals("add new item")) {

				AlertDialog.Builder builder2 = new AlertDialog.Builder(MainActivity.this);
				final EditText input = new EditText(MainActivity.this);
				input.setHeight(100);
				input.setWidth(340);
				input.setGravity(Gravity.LEFT);
				input.setImeOptions(EditorInfo.IME_ACTION_DONE);
				builder2.setMessage("Press OK or Cancel");
				builder2.setView(input);
				builder2.setPositiveButton("OK", new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						userinput = input.getText().toString();

						if (userinput.compareTo("") == 0) {
							// Your piece of code for example
							Toast toast = Toast.makeText(getApplicationContext(), "ENTER NAMES", Toast.LENGTH_SHORT);
							toast.setGravity(Gravity.CENTER | Gravity.CENTER_HORIZONTAL, 0, 0);
							toast.show();
						} else {
							listItems.add(userinput);
							mAdapter = new NavAdapter(getApplicationContext(), listItems);
							mDrawerList.setAdapter(mAdapter);
							mDrawerLayout.openDrawer(mDrawerList);
							String counter = "0";
							// Log.v("id", "" + id);
							// Log.v("name", "" + userinput);
							// Log.v("value", "" + counter);
							EditText user_message = (EditText) findViewById(R.id.editText1);
							String mes = "";
							database1.open();
							long insertid;
							insertid = database1.Insertvalues(id1, userinput, counter, mes);
						}
					}
				});

				builder2.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {

					}

				});

				builder2.show();

			}

		}
	}

	private void selectItem(int position) {

		// update the main content by replacing fragments
		Fragment fragment = new PlanetFragment();
		Bundle args = new Bundle();
		args.putInt(PlanetFragment.ARG_PLANET_NUMBER, position);
		fragment.setArguments(args);

		FragmentManager fragmentManager = getFragmentManager();
		fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).commit();

		// update selected item and title, then close the drawer
		mDrawerList.setItemChecked(position, true);
		// setTitle(mPlanetTitles[position]);
		mDrawerLayout.closeDrawer(mDrawerList);
		// incre.setVisibility(View.VISIBLE);
		// decr.setVisibility(View.VISIBLE);

	}

	@Override
	public Object onRetainNonConfigurationInstance() {
		// TODOAuto-generated method stub //
		return this.mAdapter.getItem(positn);
	}

	@Override
	public void setTitle(CharSequence title) {
		mTitle = title;
		getActionBar().setTitle(mTitle);
	}

	/**
	 * When using the ActionBarDrawerToggle, you must call it during
	 * onPostCreate() and onConfigurationChanged()...
	 */

	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
		// Sync the toggle state after onRestoreInstanceState has occurred.
		mDrawerToggle.syncState();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		// Pass any configuration change to the drawer toggls
		mDrawerToggle.onConfigurationChanged(newConfig);
	}

	/**
	 * Fragment that appears in the "content_frame", shows a planet
	 */
	public static class PlanetFragment extends Fragment {
		public static final String ARG_PLANET_NUMBER = "planet_number";
		Context c1;
		private NavAdapter mAdapter;
		public Database1 db;

		public PlanetFragment() {
			// Empty constructor required for fragment subclasses
		}

		@Override
		public void onConfigurationChanged(Configuration newConfig) {
			// TODO Auto-generated method stub
			super.onConfigurationChanged(newConfig);
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

			textValue = MainActivity.text;
			c1 = container.getContext();
			db = new Database1(c1);
			View rootView = inflater.inflate(R.layout.fragment_planet, container, false);

			// final DrawerLayout mDrawerLayout = (DrawerLayout)
			// rootView.findViewById(R.id.drawer_layout);
			string = Integer.toString(count);
			decr = (Button) rootView.findViewById(R.id.button1);
			incre = (Button) rootView.findViewById(R.id.button2);
			save = (Button) rootView.findViewById(R.id.button3);
			cancel = (Button) rootView.findViewById(R.id.button4);
			display = (TextView) rootView.findViewById(R.id.textView2);

			// incre.setVisibility(View.VISIBLE);

			user_message = (EditText) rootView.findViewById(R.id.editText1);

			save.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					String getMessage = user_message.getText().toString();
					db.open();
					c = db.getvalues();
					if (c != null) {
						if (c.moveToFirst()) {
							do {
								String string2 = c.getString(c.getColumnIndex("name"));

								if (string2.equalsIgnoreCase(textValue)) {
									String mes = user_message.getText().toString();
									long insertid3;
									insertid3 = db.InsertMessage(text, mes);
									Toast.makeText(getActivity(), "saved successfully", 1000).show();
								}
							} while (c.moveToNext());
						}
					}
				}
			});

			db.open();
			c = db.getvalues();
			if (c != null) {
				if (c.moveToFirst()) {
					do {
						String string2 = c.getString(c.getColumnIndex("name"));

						if (string2.equalsIgnoreCase(textValue)) {
							String string4 = c.getString(3);
							String mes = user_message.getText().toString();
							user_message.setText(string4);
							Log.d("mess", string4);
							long insertid3;
							insertid3 = db.UpdateMessage(text, string4);
						}
					} while (c.moveToNext());
				}
			}

			cancel.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					user_message.setText("");
					Toast.makeText(getActivity(), "content cleared", 1000).show();
				}
			});

			nub = (TextView) rootView.findViewById(R.id.textView1);
			nub.setText(string);

			db.open();
			c = db.getvalues();
			string = Integer.toString(count);
			nub.setText(string);

			if (c != null) {
				if (c.moveToFirst()) {
					do {

						String string2 = c.getString(c.getColumnIndex("name"));

						if (string2.equalsIgnoreCase(textValue)) {
							count = Integer.parseInt(c.getString(2));
							string = Integer.toString(count);
							nub.setText(string);

							decr.setOnClickListener(new OnClickListener() {

								@Override
								public void onClick(View v) {
									count--;
									string = Integer.toString(count);
									nub.setText(string);
									db.open();
									long updateid;
									updateid = db.Updatevalues(text, string);
								}
							});
							incre.setOnClickListener(new OnClickListener() {

								@Override
								public void onClick(View v) {

									count++;
									string = Integer.toString(count);
									nub.setText(string);
									db.open();
									long updateid;
									updateid = db.Updatevalues(text, string);
								}
							});
						}
					} while (c.moveToNext());
				}
			}
			return rootView;
		}
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// TODO Auto-generated method stub
		if (keyCode == KeyEvent.KEYCODE_VOLUME_DOWN) {
			count--;
			string = Integer.toString(count);
			nub.setText(string);

			database1.open();
			long updateid;
			updateid = database1.Updatevalues(text, string);
			return true;
		}
		/*
*/
		return super.onKeyDown(keyCode, event);
	}

	@Override
	public boolean onKeyUp(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_VOLUME_UP) {
			count++;
			string = Integer.toString(count);
			nub.setText(string);

			database1.open();
			long updateid;
			updateid = database1.Updatevalues(text, string);
			return true;
		}
		return super.onKeyUp(keyCode, event);
	}
}