package com.example.test_sample;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class NavAdapter extends BaseAdapter {
	private Context mContext;
	private List<String> items;

	private OnNavImageClickListener mListener;
	private OnNavImageClickListener1 mListener1;

	public NavAdapter(Context context, List<String> items) {
		this.mContext = context;
		this.items = items;
	}

	@Override
	public int getCount() {
		return items.size();
	}

	@Override
	public Object getItem(int position) {
		return items.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {

		LayoutInflater inf = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View view = inf.inflate(R.layout.rowlayout, null);
		TextView txtView = (TextView) view.findViewById(R.id.label);
		txtView.setText(items.get(position));

		ImageView imgView = (ImageView) view.findViewById(R.id.imageView1);
		imgView.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (mListener != null) {
					mListener.onClick(v, items.get(position));
				}
			}
		});

		ImageView imgView1 = (ImageView) view.findViewById(R.id.imageView2);
		imgView1.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (mListener1 != null) {
					mListener1.onClick(v, items.get(position));
				}
			}
		});

		if (position == 0) {
			imgView.setVisibility(View.INVISIBLE);
			imgView1.setVisibility(View.INVISIBLE);

		}
		return view;
	}

	public interface OnNavImageClickListener {
		public void onClick(View view, Object item);
	}

	public void setOnImageClickListener(OnNavImageClickListener listener) {
		this.mListener = listener;
	}

	public interface OnNavImageClickListener1 {
		public void onClick(View view, Object item);
	}

	public void setOnImageClickListener1(OnNavImageClickListener1 listener1) {
		this.mListener1 = listener1;
	}

}
